﻿using Gameplay;
using Gameplay.BombLogic;
using Gameplay.Bombs;
using Gameplay.Bombs.BombLogic;
using Gameplay.Generation;
using Gameplay.HeroLogic;
using GameStateModule;
using Installers;
using PrepareModules.ObjectPool;
using SimpleDIContainer.UserLayer;
using UnityEngine;

public class CompositionRoot : MonoBehaviour
{
    [SerializeField] private ConfigurationProvider _configurationProvider;

    void Start()
    {
        _configurationProvider.BombsConfig.Initialize();
        var container = new Container();
        container.BindInstance<GameStateContainer>().AsSingle().ActivateNonLazy();
        new UiInstaller(container).InstallDependencies();
        container.Bind<IRandomTypeSelectionAlgorithm, RandomTypeSelectionWithChanceAlgorithm>().AsSingle()
            .SetToConstructor(_configurationProvider.ChanceConfig);
        container.BindInstance<ObjectVisualPathFinder>().AsSingle();
        new BombInstaller(container, _configurationProvider).InstallDependencies();
        container.Bind<IObjectVisualCreator, ObjectVisualCreator>().AsSingle();
        container.Bind<IObjectsMapGenerator, ObjectsMapGenerator>().AsSingle();
        container.BindInstance<HeroInitializator>().AsSingle().SetToConstructor(_configurationProvider.HeroConfig)
            .ActivateNonLazy();
        container.BindInstance<GameLoader>().AsSingle().SetToConstructor(_configurationProvider).ActivateNonLazy();
    }
}
