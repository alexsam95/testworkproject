﻿namespace GameStateModule
{
    public enum GameState : short
    {
        InMenu,
        InGame
    }
}
