﻿using UniRx;

namespace GameStateModule
{
    public class GameStateContainer
    {
        public ReactiveProperty<GameState> GameState { get; } =
            new ReactiveProperty<GameState>(GameStateModule.GameState.InMenu);
    }
}
