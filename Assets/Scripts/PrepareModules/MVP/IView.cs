namespace  PrepareModules.MVP
{
    public interface IView <in T> : IBaseView
    {
        void SetPresenter(T presenter);
    }
}