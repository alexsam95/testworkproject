namespace PrepareModules.MVP
{
    public class BasePresenter<T> : IPresenter<T> where T : IBaseView
    {
        public PopupType PopupType { get; }
        protected T View;

        public void Show()
        {
            View.Show();
            OnShowed();
        }

        public void Hide()
        {
            View.Hide();
        }

        public virtual void SetView(T view)
        {
            View = view;
        }

        /// <summary>
        /// After show view
        /// </summary>
        public virtual void OnShowed()
        {

        }

        /// <summary>
        /// After initialized presenter, before show
        /// </summary>
        public virtual void OnInitialized()
        {

        }

        protected BasePresenter(PopupType type)
        {
            PopupType = type;
        }
    }
}