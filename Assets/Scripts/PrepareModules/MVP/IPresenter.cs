namespace PrepareModules.MVP
{
    public interface IPresenter<in T>: IBasePresenter where T : IBaseView
    {
        void SetView(T view);
    }
}