﻿namespace PrepareModules.MVP
{
    public interface IBaseView 
    {
        void SubscribeButtons();
        void OnInitialized();
        void Show();
        void Hide();
    }
}
