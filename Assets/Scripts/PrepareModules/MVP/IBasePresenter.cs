﻿
namespace PrepareModules.MVP
{
    public interface IBasePresenter 
    {
        PopupType PopupType { get; }
        void OnInitialized();
        void Show();
        void Hide();
    }
}
