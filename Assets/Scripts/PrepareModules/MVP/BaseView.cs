using UnityEngine;

namespace PrepareModules.MVP
{
    public abstract class BaseView<T> : MonoBehaviour, IView<T>
    {
        protected T Presenter;

        public void SetPresenter(T presenter)
        {
            Presenter = presenter;
        }

        public virtual void SubscribeButtons()
        {
        }
        /// <summary>
        /// After initialized view, before show view
        /// </summary>
        public virtual void OnInitialized()
        {
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}