using UnityEngine;

namespace PrepareModules.MVP
{
    public interface IPopupConstructor
    {
        (T view,TV presenter) ConstructPopup<T, TV>(string prefabPath, GameObject parent, TV presenter)
            where T : class, IView<TV> where TV : class, IPresenter<T>;
    }
}