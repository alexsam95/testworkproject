using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace PrepareModules.MVP
{
    public class PopupConstructor : IPopupConstructor
    {
        public (T view, TV presenter) ConstructPopup<T, TV>(string prefabPath, GameObject parent,
            TV presenter)
            where T : class, IView<TV> where TV : class, IPresenter<T>
        {
            var resource = Resources.Load(prefabPath);
            var gameObject = Object.Instantiate(resource, parent.transform, false) as GameObject;
            if (gameObject == null) throw new NullReferenceException("error with creating view by path:" + prefabPath);
            var view = gameObject.GetComponent<T>();
            if (view == null || presenter == null)
            {
                throw new NullReferenceException("error with type:" + view + " and " + presenter);
            }

            view.SetPresenter(presenter);
            presenter.SetView(view);
            view.OnInitialized();
            presenter.OnInitialized();
            view.SubscribeButtons();
            return (view, presenter);
        }
    }
}