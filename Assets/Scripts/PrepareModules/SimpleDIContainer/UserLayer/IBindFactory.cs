﻿using System;

namespace SimpleDIContainer.UserLayer
{
    public interface IBindFactory
    {
        Type GetDeclaredType();
        object GetConstructedObject();
    }
}