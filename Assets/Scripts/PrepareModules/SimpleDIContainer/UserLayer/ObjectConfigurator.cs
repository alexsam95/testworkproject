using SimpleDIContainer.CoreLogic.CommandLogic;
using SimpleDIContainer.CoreLogic.Resolvers;

namespace SimpleDIContainer.UserLayer
{
    public class ObjectConfigurator
    {
        private readonly ICommandController _commandController;

        internal ObjectConfigurator(ICommandController commandController)
        {
            _commandController = commandController;
        }

        /// <summary>
        /// Bind property in class. Container find property with type T and set dependency 
        /// </summary>
        public ObjectConfigurator BindProperty(string propertyName)
        {
            _commandController.AddPropertyTypeForBindToLastCommand(propertyName);
            return this;
        }

        /// <summary>
        /// Bind all property in class
        /// </summary>
        public ObjectConfigurator BindAllProperties()
        {
            _commandController.SetPropertyResolveModeToLastCommand(PropertyResolveMode.All);
            return this;
        }

        /// <summary>
        /// Bind method arguments. Container find method by methodname and get all dependency by argument
        /// </summary>
        public ObjectConfigurator BindMethod(string methodName)
        {
            _commandController.AddMethodNameForBindToLastCommand(methodName);
            return this;
        }

        /// <summary>
        /// Set configuration. Transient strategy: for each appeal container create a new instance 
        /// </summary>
        public ObjectConfigurator AsTransient()
        {
            _commandController.SetLifecycleTypeToLastCommand(LifecycleType.Transient);
            return this;
        }

        /// <summary>
        /// Set configuration. Transient strategy: for each appeal container returned one(cached) instance 
        /// </summary>
        public ObjectConfigurator AsSingle()
        {
            _commandController.SetLifecycleTypeToLastCommand(LifecycleType.Singleton);
            return this;
        }

        /// <summary>
        ///  Create this instance now 
        /// </summary>
        public void ActivateNonLazy()
        {
            _commandController.RunCommands();
        }

        /// <summary>
        ///  Manual (without container) add objects to constructor after container create this instance
        /// </summary>
        public ObjectConfigurator SetToConstructor(params object[] parameters)
        {
            _commandController.AddObjectsForConstructorToLastCommand(parameters);
            return this;
        }

        /// <summary>
        ///  Manual (without container) add objects to method's argument (find by method name) after container
        /// create this instance
        /// </summary>
        public ObjectConfigurator SetToMethod(string methodName, params object[] parameters)
        {
            _commandController.AddObjectsForMethodToLastCommand(methodName, parameters);
            return this;
        }

        /// <summary>
        /// Manual (without container) add objects to property (find by property name) after container
        /// create this instance
        /// </summary>
        /// <returns></returns>
        public ObjectConfigurator SetToProperty(string propertyName, object parameter)
        {
            _commandController.AddObjectsForPropertyToLastCommand(propertyName, parameter);
            return this;
        }
    }
}