﻿using System;
using SimpleDIContainer.CoreLogic.CommandLogic;
using SimpleDIContainer.SystemLayer;

namespace SimpleDIContainer.UserLayer
{
    public class Container : IDisposable
    {
        private readonly AppConfigurator _appConfigurator = new AppConfigurator();

        private ICommandController CommandController => _appConfigurator.GetCoreLogicProvider().GetCommander();
        private ObjectConfigurator _objectConfigurator;

        public Container()
        {
            _objectConfigurator = new ObjectConfigurator(CommandController);
        }

        /// <summary>
        /// Bind class as interface.The container gives this dependency on the interface type
        /// </summary>
        public ObjectConfigurator Bind<TAbstractType, TConcreteType>() where TConcreteType : class, TAbstractType
        {
            CommandController.AddCommand(typeof(TAbstractType), typeof(TConcreteType));
            return _objectConfigurator;
        }

        /// <summary>
        /// Bind class. The container gives this dependency on the class type
        /// </summary>
        public ObjectConfigurator BindInstance<T>()
        {
            CommandController.AddCommand(typeof(T), typeof(T));
            return _objectConfigurator;
        }

        /// <summary>
        /// use this for register custom factory(returned object set to container)
        /// </summary>
        public ObjectConfigurator SetFactory<T>()
        {
            CommandController.AddCommand(typeof(T), typeof(T));
            return _objectConfigurator;
        }

        /// <summary>
        /// manual clear resources(all created dependencies)
        /// </summary>
        public void Dispose()
        {
            _objectConfigurator = null;
            _appConfigurator.Dispose();
        }
    }
}