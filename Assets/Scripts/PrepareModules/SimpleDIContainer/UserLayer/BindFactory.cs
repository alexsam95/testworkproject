using System;

namespace SimpleDIContainer.UserLayer
{
    /// <summary>
    /// Need override ConstructObject 
    /// </summary>
    public abstract class BindFactory <T> : IBindFactory
    {
        public Type GetDeclaredType()
        {
            return typeof(T);
        }
        public object GetConstructedObject()
        {
            return ConstructObject();
        }

        protected abstract T ConstructObject();
    }
}