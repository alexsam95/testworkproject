namespace SimpleDIContainer.UserLayer
{
    public enum LifecycleType : short
    {
        Singleton,
        Transient
    }
}