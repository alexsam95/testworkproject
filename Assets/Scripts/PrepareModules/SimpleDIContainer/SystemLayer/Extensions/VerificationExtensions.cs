using System;
using System.Linq;

namespace SimpleDIContainer.SystemLayer.Extensions
{
    static class VerificationExtensions
    {
        public static void CheckParametersForNull(Action ifNullAction, params object[] objects)
        {
            if (objects.All(obj => obj != null)) return;
            ifNullAction?.Invoke();
        }

        public static void CheckParametersForPredicate(Predicate<object> checkedPredicate,
            Action actionIfFalsePredicate,
            params object[] objects)
        {
            if (!objects.All(obj => checkedPredicate(obj)))
            {
                actionIfFalsePredicate?.Invoke();
            }
        }
    }
}