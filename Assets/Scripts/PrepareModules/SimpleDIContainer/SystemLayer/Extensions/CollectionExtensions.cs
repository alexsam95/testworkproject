using System;
using System.Collections.Generic;

namespace SimpleDIContainer.SystemLayer.Extensions
{
    static class CollectionExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> collection, Action<T> action)
        {
            if (collection == null || action == null) return;
            foreach (var collectionMember in collection)
            {
                action.Invoke(collectionMember);
            }
        }

        public static bool HasOnlyOneMember<T>(this ICollection<T> collection)
        {
            return collection.Count == 1;
        }

        public static object CreateCollectionWithType(Type type, Type genericType)
        {
            return Activator.CreateInstance(type
                .MakeGenericType(genericType.GetGenericArguments()));
        }
    }
}