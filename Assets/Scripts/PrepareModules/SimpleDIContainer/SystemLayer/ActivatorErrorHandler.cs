using System;

namespace SimpleDIContainer.SystemLayer
{
    class ActivatorErrorHandler : IActivatorErrorHandler
    {
        public object CreateObjectWithHandleError(Type type, Func<object> instantiateFunc,
            Func<string> createErrorInfo = null)
        {
            try
            {
                return instantiateFunc.Invoke();
            }
            catch (MissingMethodException)
            {
                throw new ContainerException("Error with creating:" + type +
                                             "-incorrect constructors params(bind and injected types are different) error info:\n"
                                             + createErrorInfo?.Invoke(), true);
            }
            catch (NullReferenceException)
            {
                throw new ContainerException("Error with call constructor in object:" +
                                             type + " null reference:" + createErrorInfo?.Invoke(), false);
            }
        }
    }
}