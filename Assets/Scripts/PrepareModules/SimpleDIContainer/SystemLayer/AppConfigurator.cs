using System;
using SimpleDIContainer.CoreLogic;
using SimpleDIContainer.DataLayer;

namespace SimpleDIContainer.SystemLayer
{
    class AppConfigurator : IDisposable
    {
        private IDataLayerProvider _dataLayerProvider;
        private ICoreLogicProvider _coreLogicProvider;

        public IDataLayerProvider GetDataLayerProvider()
        {
            if (_dataLayerProvider == null)
            {
                _dataLayerProvider = new DataLayerProvider();
            }

            return _dataLayerProvider;
        }

        public ICoreLogicProvider GetCoreLogicProvider()
        {
            if (_coreLogicProvider == null)
            {
                _coreLogicProvider = new CoreLogicProvider(GetDataLayerProvider());
            }

            return _coreLogicProvider;
        }

        public void Dispose()
        {
            _coreLogicProvider.Dispose();
            _dataLayerProvider.Dispose();
        }
    }
}