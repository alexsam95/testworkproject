using System;

namespace SimpleDIContainer.SystemLayer
{
    interface IActivatorErrorHandler
    {
        object CreateObjectWithHandleError(Type type, Func<object> instantiateFunc,
            Func<string> createErrorInfo = null);
    }
}