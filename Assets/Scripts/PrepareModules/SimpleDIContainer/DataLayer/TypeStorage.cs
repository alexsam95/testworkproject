using System;
using System.Collections.Generic;
using System.Linq;
using SimpleDIContainer.CoreLogic;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.DataLayer
{
    class TypeStorage : ITypesStorage<Type, ObjectDataContainer>
    {
        private readonly Dictionary<Type, IList<ObjectDataContainer>> _storage =
            new Dictionary<Type, IList<ObjectDataContainer>>();

        public void Add(Type type, ObjectDataContainer objectDataContainer)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException("Attempt add null data to storage",
                false), objectDataContainer);
            if (_storage.ContainsKey(type))
            {
                AddRealizationToOtherRealizations(type, objectDataContainer);
                return;
            }

            _storage.Add(type, new List<ObjectDataContainer> {objectDataContainer});
        }

        public IList<ObjectDataContainer> GetObjectContainers(Type type)
        {
            if (!TypeIsIEnumerableImplement(type))
            {
                return _storage.ContainsKey(type) ? _storage[type] : null;
            }

            var key = GenerateKey(type);
            return _storage.ContainsKey(key) ? _storage[key] : null;
        }

        public bool Contains(Type type)
        {
            return _storage.ContainsKey(!TypeIsIEnumerableImplement(type)
                ? type
                : GenerateKey(type));
        }

        public void Clear()
        {
            _storage.Clear();
        }

        private void AddRealizationToOtherRealizations(Type type, ObjectDataContainer objectDataContainer)
        {
            var objectContainers = _storage[type];
            objectContainers.Add(objectDataContainer);
            var key = GenerateKey(type);
            if (!_storage.ContainsKey(key))
            {
                _storage.Add(key, objectContainers);
            }
            else
            {
                _storage[key] = objectContainers;
            }
        }

        private Type GenerateKey(Type type)
        {
            return !type.IsGenericTypeDefinition ? type : type.MakeGenericType(type.GetGenericArguments());
        }

        private bool HasGenericType(Type type)
        {
            return type.GetGenericArguments().Any();
        }

        private bool TypeIsIEnumerableImplement(Type type)
        {
            return HasGenericType(type)
                   && type.GetInterfaces().Contains(GenerateKey(type));
        }
    }
}