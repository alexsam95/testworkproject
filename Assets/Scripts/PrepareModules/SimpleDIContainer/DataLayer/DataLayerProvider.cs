using System;
using SimpleDIContainer.CoreLogic;

namespace SimpleDIContainer.DataLayer
{
    class DataLayerProvider : IDataLayerProvider
    {
        private ITypesStorage<Type, ObjectDataContainer> _storage;

        public ITypesStorage<Type, ObjectDataContainer> GetStorage()
        {
            if (_storage != null) return _storage;
            _storage = new TypeStorage();
            return _storage;
        }

        public void Dispose()
        {
            _storage.Clear();
        }
    }
}