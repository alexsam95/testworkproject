using System.Collections.Generic;
using SimpleDIContainer.CoreLogic;

namespace SimpleDIContainer.DataLayer
{
    interface ITypesStorage<in T, TObjectContainer> where TObjectContainer : ObjectDataContainer
    {
        void Add(T type, TObjectContainer objectMediator);
        IList<TObjectContainer> GetObjectContainers(T type);
        bool Contains(T type);
        void Clear();
    }
}