using System;
using SimpleDIContainer.CoreLogic;

namespace SimpleDIContainer.DataLayer
{
    interface IDataLayerProvider : IDisposable
    {
        ITypesStorage<Type, ObjectDataContainer> GetStorage();
    }
}