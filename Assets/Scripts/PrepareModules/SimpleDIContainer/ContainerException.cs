using System;

namespace SimpleDIContainer
{
    public class ContainerException : ApplicationException
    {
        public ContainerException(string message, bool isUserException)
            : base($"{UserOrContainerExceptionStringConstruct(isUserException) + message}")
        {
        }

        private static string UserOrContainerExceptionStringConstruct(bool isUserException)
        {
            return isUserException ? "UserException:" : "DI Container Exception";
        }
    }
}