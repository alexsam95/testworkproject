using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic
{
    class ObjectDataContainer
    {
        public List<object> ObjectsForConstructor { get; } = new List<object>();
        public Dictionary<string, List<object>> ObjectsForMethod { get; } = new Dictionary<string, List<object>>();
        public Dictionary<string, object> ObjectsForProperty { get; } = new Dictionary<string, object>();
        public ILifecycleStrategy LifecycleStrategy { internal get; set; }
        public ResolveParameters ResolveParameters { get; }

        public object ObjectInstance => _objectInstance ??
                                        LifecycleStrategy?.GetInstance(_concreteObjectType, this);

        private readonly Type _concreteObjectType;
        private readonly object _objectInstance;

        public ObjectDataContainer(Type type, ResolveParameters resolveParameters)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException(
                "Null arguments to constructor ObjectDataContainer"
                , false), type, resolveParameters);
            _concreteObjectType = type;
            ResolveParameters = resolveParameters;
        }
        public ObjectDataContainer(object instance)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException("Null instance after create", false),
                instance);
            _objectInstance = instance;
            _concreteObjectType = instance.GetType();
        }

    }
}