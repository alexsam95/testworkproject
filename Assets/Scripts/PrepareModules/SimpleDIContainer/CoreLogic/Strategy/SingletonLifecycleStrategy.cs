using System;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic.Strategy
{
    class SingletonLifecycleStrategy : ILifecycleStrategy
    {
        private object _cachedObject;
        private readonly IActivatorService _activatorService;

        public SingletonLifecycleStrategy(IActivatorService activatorService)
        {
            VerificationExtensions.CheckParametersForNull(() =>
                throw new ContainerException("In SingletonLifecycleStrategy constructor null arguments"
                    , false), activatorService);
            _activatorService = activatorService;
        }

        public object GetInstance(Type type, ObjectDataContainer objectDataContainer)
        {
            if (_cachedObject != null) return _cachedObject;
            _cachedObject = _activatorService.CreateObject(type, objectDataContainer);
            return _cachedObject;
        }
    }
}