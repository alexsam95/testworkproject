using SimpleDIContainer.SystemLayer.Extensions;
using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.Strategy
{
    class LifeCycleStrategyFactory : ILifecycleStrategyFactory
    {
        private readonly IActivatorService _activatorService;

        public LifeCycleStrategyFactory(IActivatorService activatorService)
        {
            VerificationExtensions.CheckParametersForNull(() =>
                throw new ContainerException("In LifeCycleStrategyFactory constructor null arguments"
                    , false), activatorService);
            _activatorService = activatorService;
        }

        public ILifecycleStrategy GetStrategy(LifecycleType lifecycleType)
        {
            switch (lifecycleType)
            {
                case LifecycleType.Singleton:
                    return new SingletonLifecycleStrategy(_activatorService);
                case LifecycleType.Transient:
                    return new TransientLifecycleStrategy(_activatorService);
                default:
                    return new SingletonLifecycleStrategy(_activatorService);
            }
        }

    }
}