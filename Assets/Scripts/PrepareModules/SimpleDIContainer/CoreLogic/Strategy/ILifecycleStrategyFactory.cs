using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.Strategy
{
    interface ILifecycleStrategyFactory
    {
        ILifecycleStrategy GetStrategy(LifecycleType lifecycleType);
    }
}