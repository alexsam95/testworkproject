using System;

namespace SimpleDIContainer.CoreLogic.Strategy
{
    interface ILifecycleStrategy
    {
        object GetInstance(Type type, ObjectDataContainer objectDataContainer);
    }
}