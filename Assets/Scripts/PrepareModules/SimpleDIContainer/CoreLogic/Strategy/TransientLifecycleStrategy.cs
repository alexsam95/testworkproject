using System;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic.Strategy
{
    class TransientLifecycleStrategy : ILifecycleStrategy
    {
        private readonly IActivatorService _activatorService;

        public TransientLifecycleStrategy(IActivatorService activatorService)
        {
            VerificationExtensions.CheckParametersForNull(() =>
                throw new ContainerException("In TransientLifecycleStrategy constructor null arguments"
                    , false), activatorService);
            _activatorService = activatorService;
        }

        public object GetInstance(Type type, ObjectDataContainer objectDataContainer)
        {
            return _activatorService.CreateObject(type, objectDataContainer);
        }
    }
}