using System;
using System.Collections.Generic;
using System.Reflection;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    class MethodPreInstanceResolver : BaseResolver, IPostInstanceResolver
    {
        private readonly ITypesStorage<Type, ObjectDataContainer> _storage;

        public MethodPreInstanceResolver(ITypesStorage<Type, ObjectDataContainer> storage)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException
                ("In MethodPreInstanceResolver constructor null argument", false), storage);
            _storage = storage;
            _storage = storage;
        }

        public void Resolve(object concreteObj, ObjectDataContainer objectDataContainer)
        {
            objectDataContainer.ResolveParameters.MethodNames.ForEach(methodName =>
            {
                var method = concreteObj.GetType().GetMethod(methodName);
                if (method == null) return;
                if (PreparedObjectIsSetted(method, objectDataContainer, concreteObj))
                {
                    return;
                }

                method.Invoke(concreteObj, GetObjects(method.GetParameters()));
            });
        }

        private bool PreparedObjectIsSetted(MethodInfo methodInfo, ObjectDataContainer objectDataContainer,
            object concreteObj)
        {
            var objectsForPropertySet = objectDataContainer.ObjectsForMethod;
            if (!objectsForPropertySet.ContainsKey(methodInfo.Name)) return false;
            var value = objectsForPropertySet[methodInfo.Name];
            methodInfo.Invoke(concreteObj, value.ToArray());
            return true;
        }

        private object[] GetObjects(IReadOnlyList<ParameterInfo> parameterInfo)
        {
            var resultArray = new object[parameterInfo.Count];
            for (var index = 0; index < parameterInfo.Count; index++)
            {
                if (!_storage.Contains(parameterInfo[index].ParameterType)) return resultArray;
                var objectContainers = _storage.GetObjectContainers(parameterInfo[index].ParameterType);
                switch (objectContainers.Count)
                {
                    case 0:
                        throw new ContainerException("for " + parameterInfo[index].ParameterType
                                                            + "no found value in bind objects", true);
                    case 1:
                        resultArray[index] = objectContainers[0].ObjectInstance;
                        continue;
                    default:
                        resultArray[index] =
                            CreateListWithSeveralRealizations(parameterInfo[index].ParameterType, objectContainers);
                        break;
                }
            }

            return resultArray;
        }
    }
}