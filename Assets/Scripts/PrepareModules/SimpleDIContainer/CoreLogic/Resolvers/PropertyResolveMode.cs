namespace SimpleDIContainer.CoreLogic.Resolvers
{
    public enum PropertyResolveMode : short
    {
        All,
        Manual
    }
}