using System;
using System.Collections.Generic;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    class ResolveParameters
    {
        public PropertyResolveMode PropertyResolveMode { get; set; } = PropertyResolveMode.Manual;
        public IList<string> PropertyTypeList { get; } = new List<string>();
        public IList<string> MethodNames { get; } = new List<string>();
    }
}