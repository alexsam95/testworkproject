using System;
using System.Collections.Generic;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    interface IPreInstanceResolver : IDisposable
    {
        IEnumerable<object> Resolve(Type type, ObjectDataContainer objectDataContainer);
    }
}