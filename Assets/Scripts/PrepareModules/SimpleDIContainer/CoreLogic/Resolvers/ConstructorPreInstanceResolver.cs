using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    class ConstructorPreInstanceResolver : BaseResolver, IPreInstanceResolver
    {
        private readonly ITypesStorage<Type, ObjectDataContainer> _storage;

        internal ConstructorPreInstanceResolver(ITypesStorage<Type, ObjectDataContainer> storage)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException
                    ("In ConstructorPreInstanceResolver constructor null argument", false),
                storage);
            _storage = storage;
        }

        public IEnumerable<object> Resolve(Type type, ObjectDataContainer objectDataContainer)
        {
            var constructorInfo = FindConstructor(type.GetConstructors());
            var resultList = CreateResultList(constructorInfo, objectDataContainer);
            foreach (var parameter in constructorInfo.GetParameters())
            {
                var objectContainers =
                    _storage.GetObjectContainers(parameter.ParameterType);
                if (objectContainers == null)
                {
                    continue;
                }
                switch (objectContainers.Count)
                {
                    case 0:
                        throw new ContainerException(
                            "for " + parameter.ParameterType + "no found value in bind objects", true);
                    case 1:
                        resultList.Add(objectContainers[0].ObjectInstance);
                        break;
                    default:
                        resultList.Add(CreateListWithSeveralRealizations(parameter.ParameterType, objectContainers));
                        break;
                }
            }

            return resultList;
        }

        private List<object> CreateResultList(ConstructorInfo constructorInfo, ObjectDataContainer objectDataContainer)
        {
            var resultList = new List<object>();
            if (objectDataContainer.ObjectsForConstructor == null || !constructorInfo.GetParameters().Any())
            {
                return resultList;
            }

            resultList.AddRange(objectDataContainer.ObjectsForConstructor);
            return resultList;
        }

        private ConstructorInfo FindConstructor(IList<ConstructorInfo> constructorInfoArray)
        {
            foreach (var constructor in constructorInfoArray)
            {
                if (AllTypesInStorage(constructor.GetParameters()))
                {
                    return constructor;
                }
            }

            return constructorInfoArray.FirstOrDefault();
        }

        private bool AllTypesInStorage(IEnumerable<ParameterInfo> parameters)
        {
            return parameters.All(parameter => !_storage.Contains(parameter.ParameterType));
        }

        public void Dispose()
        {
        }
    }
}