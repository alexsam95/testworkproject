using System;
using System.Collections;
using System.Collections.Generic;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    class BaseResolver
    {
        protected object CreateListWithSeveralRealizations(Type type,
            IEnumerable<ObjectDataContainer> objectDataContainers)
        {
            var sublist = CollectionExtensions.CreateCollectionWithType(typeof(List<>), type);
            if (sublist is IList listForAddedValue)
            {
                objectDataContainers.ForEach(container => { listForAddedValue.Add(container.ObjectInstance); });
            }

            return sublist;
        }
    }
}