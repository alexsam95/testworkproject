namespace SimpleDIContainer.CoreLogic.Resolvers
{
    interface IPostInstanceResolver
    {
        void Resolve(object concreteObj, ObjectDataContainer objectDataContainer);
    }
}