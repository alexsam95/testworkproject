using System;
using System.Collections.Generic;
using System.Reflection;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic.Resolvers
{
    class PropertyPreInstanceResolver : BaseResolver, IPostInstanceResolver
    {
        private readonly ITypesStorage<Type, ObjectDataContainer> _storage;

        public PropertyPreInstanceResolver(ITypesStorage<Type, ObjectDataContainer> storage)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException
                ("In PropertyPreInstanceResolver constructor null argument", false), storage);
            _storage = storage;
        }

        public void Resolve(object concreteObj, ObjectDataContainer objectDataContainer)
        {
            var properties = concreteObj.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);
            var resolveType = objectDataContainer.ResolveParameters.PropertyResolveMode;
            var resolveList = objectDataContainer.ResolveParameters.PropertyTypeList;
            properties.ForEach(property =>
            {
                if (PreparedObjectIsSetted(property, objectDataContainer, concreteObj))
                {
                    return;
                }

                if (!_storage.Contains(property.PropertyType) ||
                    NoBindedTypeInManualMode(property, resolveType, resolveList)) return;
                SetValueToProperty(property, concreteObj);
            });
        }

        private bool PreparedObjectIsSetted(PropertyInfo propertyInfo, ObjectDataContainer objectDataContainer,
            object concreteObj)
        {
            var objectsForPropertySet = objectDataContainer.ObjectsForProperty;
            if (!objectsForPropertySet.ContainsKey(propertyInfo.Name)) return false;
            var value = objectsForPropertySet[propertyInfo.Name];
            propertyInfo.SetValue(concreteObj, value);
            return true;
        }

        private void SetValueToProperty(PropertyInfo property, object value)
        {
            var objectContainers = _storage.GetObjectContainers(property.PropertyType);
            if (objectContainers.HasOnlyOneMember())
            {
                property.SetValue(value, objectContainers[0].ObjectInstance);
                return;
            }

            property.SetValue(value, CreateListWithSeveralRealizations(property.PropertyType, objectContainers));
        }

        private bool NoBindedTypeInManualMode(PropertyInfo property, PropertyResolveMode resolveMode,
            ICollection<string> resolveList)
        {
            return resolveMode == PropertyResolveMode.Manual && !resolveList.Contains(property.Name);
        }
    }
}