using System;

namespace SimpleDIContainer.CoreLogic
{
    interface IActivatorService
    {
        object CreateObject(Type type, ObjectDataContainer objectDataContainer);
    }
}