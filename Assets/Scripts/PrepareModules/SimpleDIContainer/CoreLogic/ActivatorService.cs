using System;
using System.Collections.Generic;
using System.Text;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.SystemLayer;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic
{
    class ActivatorService : IActivatorService
    {
        private readonly IEnumerable<IPreInstanceResolver> _preInstanceResolvers;
        private readonly IEnumerable<IPostInstanceResolver> _postInstanceResolvers;
        private readonly IActivatorErrorHandler _activatorErrorHandler;

        internal ActivatorService(IEnumerable<IPreInstanceResolver> preInstanceResolvers,
            IEnumerable<IPostInstanceResolver> postInstanceResolvers,
            IActivatorErrorHandler activatorErrorHandler = null)
        {
            _postInstanceResolvers = postInstanceResolvers;
            _preInstanceResolvers = preInstanceResolvers;
            _activatorErrorHandler = activatorErrorHandler;
        }

        public object CreateObject(Type type, ObjectDataContainer objectDataContainer)
        {
            var parameters = new List<object>();
            _preInstanceResolvers.ForEach(preInstanceResolver =>
            {
                parameters.AddRange(preInstanceResolver.Resolve(type, objectDataContainer));
            });
            var resultObject = CreateObject(type, parameters);
            VerificationExtensions.CheckParametersForNull(() =>
                    throw new ContainerException("ActivatorService can't create object", false),
                resultObject);
            _postInstanceResolvers.ForEach(postInstanceResolver =>
            {
                postInstanceResolver.Resolve(resultObject, objectDataContainer);
            });
            return resultObject;
        }

        private object CreateObject(Type type, List<object> parameters)
        {
            var createInstanceAction = new Func<object>(() => Activator.CreateInstance(type, parameters.ToArray()));
            if (_activatorErrorHandler == null)
            {
                return createInstanceAction.Invoke();
            }

            return _activatorErrorHandler.CreateObjectWithHandleError(type,
                () => createInstanceAction.Invoke(),
                () => PrepareErrorInfo(parameters));
        }

        private string PrepareErrorInfo(List<object> parameters)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.Append("property list:\n");
            parameters.ForEach(value => stringBuilder.Append(value).Append(",\n"));
            return stringBuilder.ToString();
        }
    }
}