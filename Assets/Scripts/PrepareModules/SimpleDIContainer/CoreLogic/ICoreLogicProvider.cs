using System;
using SimpleDIContainer.CoreLogic.CommandLogic;
using SimpleDIContainer.CoreLogic.Strategy;

namespace SimpleDIContainer.CoreLogic
{
    interface ICoreLogicProvider : IDisposable
    {
        IActivatorService GetActivatorService();
        ILifecycleStrategyFactory GetLifecycleController();
        ICommandController GetCommander();
    }
}