using System;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    interface ICommandController : IDisposable
    {

        void AddCommand(Type abstractType, Type concreteType);
        void AddPropertyTypeForBindToLastCommand(string propertyName);
        void AddMethodNameForBindToLastCommand(string methodName);
        void SetLifecycleTypeToLastCommand(LifecycleType type);
        void SetPropertyResolveModeToLastCommand(PropertyResolveMode mode);
        void AddObjectsForConstructorToLastCommand(params object[] parameters);
        void AddObjectsForMethodToLastCommand(string methodName, params object[] parameters);
        void AddObjectsForPropertyToLastCommand(string propertyName, object parameters);
        void RunCommands();
    }
}