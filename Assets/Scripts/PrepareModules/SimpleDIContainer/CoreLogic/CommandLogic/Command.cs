using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    class Command : ICommand
    {
        private readonly ObjectDataContainer _objectDataContainer;
        private readonly ITypesStorage<Type, ObjectDataContainer> _typesStorage;
        private readonly Type _abstractType;

        internal Command(Type abstractType, ITypesStorage<Type, ObjectDataContainer> storage,
            ObjectDataContainer objectDataContainer, ILifecycleStrategy defaultLifecycleStrategy)
        {
            VerificationExtensions.CheckParametersForNull(() =>
                    throw new ContainerException("Command null arguments in constructor", false),
                abstractType, storage, objectDataContainer);
            _abstractType = abstractType;
            _typesStorage = storage;
            _objectDataContainer = objectDataContainer;
            if (_objectDataContainer.LifecycleStrategy == null)
            {
                _objectDataContainer.LifecycleStrategy = defaultLifecycleStrategy;
            }
        }

        public string PropertyBindName
        {
            set => _objectDataContainer.ResolveParameters?.PropertyTypeList.Add(value);
        }

        public string BindMethodName
        {
            set => _objectDataContainer.ResolveParameters?.MethodNames.Add(value);
        }

        public PropertyResolveMode PropertyResolveMode
        {
            set => _objectDataContainer.ResolveParameters.PropertyResolveMode = value;
        }

        public ILifecycleStrategy LifecycleStrategy
        {
            set => _objectDataContainer.LifecycleStrategy = value;
        }

        public void AddObjectsForConstructor(params object[] parameters)
        {
            _objectDataContainer.ObjectsForConstructor.AddRange(parameters);
        }

        public void AddObjectsForMethod(string methodName, params object[] parameters)
        {
            var objectsList = new List<object>();
            objectsList.AddRange(parameters);
            _objectDataContainer.ObjectsForMethod.Add(methodName, objectsList);
            _objectDataContainer.ResolveParameters.MethodNames.Add(methodName);
        }

        public void AddObjectsForProperty(string propertyName, object parameter)
        {
            _objectDataContainer.ObjectsForProperty.Add(propertyName, parameter);
            _objectDataContainer.ResolveParameters.MethodNames.Add(propertyName);
        }

        public void Invoke(bool withInstance)
        {
            _typesStorage.Add(_abstractType, _objectDataContainer);
            if (withInstance)
            {
                var unused = _objectDataContainer.ObjectInstance;
            }
        }
    }
}