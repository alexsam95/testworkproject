using System;
using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer.Extensions;
using SimpleDIContainer.UserLayer;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    class CommandController : ICommandController
    {
        private readonly List<ICommand> _commands = new List<ICommand>();
        private readonly ITypesStorage<Type, ObjectDataContainer> _storage;
        private readonly ILifecycleStrategyFactory _lifecycleStrategyFactory;
        private ICommand _inProgressCommand = new EmptyCommand();

        internal CommandController(ITypesStorage<Type, ObjectDataContainer> storage,
            ILifecycleStrategyFactory lifecycleStrategyFactory)
        {
            VerificationExtensions.CheckParametersForNull(() => throw new ContainerException
                    ("null arguments " + "in constructorCommandController", false),
                storage, lifecycleStrategyFactory);
            _lifecycleStrategyFactory = lifecycleStrategyFactory;
            _storage = storage;
        }

        public void RunCommands()
        {
            _commands.ForEach(command => { command.Invoke(command == _inProgressCommand); });
            _inProgressCommand = new EmptyCommand();
            _commands.Clear();
        }

        public void AddCommand(Type abstractType, Type concreteType)
        {
            var command = new Command(abstractType, _storage,
                new ObjectDataContainer(concreteType,
                    new ResolveParameters()), _lifecycleStrategyFactory.GetStrategy(LifecycleType.Singleton));
            _commands.Add(command);
            _inProgressCommand = command;
        }


        public void SetLifecycleTypeToLastCommand(LifecycleType type)
        {
            _inProgressCommand.LifecycleStrategy = _lifecycleStrategyFactory.GetStrategy(type);
        }

        public void SetPropertyResolveModeToLastCommand(PropertyResolveMode mode)
        {
            _inProgressCommand.PropertyResolveMode = mode;
        }

        public void AddObjectsForConstructorToLastCommand(params object[] parameters)
        {
            _inProgressCommand.AddObjectsForConstructor(parameters);
        }

        public void AddObjectsForMethodToLastCommand(string methodName, params object[] parameters)
        {
            _inProgressCommand.AddObjectsForMethod(methodName, parameters);
        }
        public void AddPropertyTypeForBindToLastCommand(string propertyName)
        {
            _inProgressCommand.PropertyBindName = propertyName;
        }
        public void AddMethodNameForBindToLastCommand(string methodName)
        {
            _inProgressCommand.BindMethodName = methodName;
        }

        public void AddObjectsForPropertyToLastCommand(string propertyName, object parameter)
        {
            _inProgressCommand.AddObjectsForProperty(propertyName, parameter);
        }

        public void Dispose()
        {
            _inProgressCommand = null;
            _commands.Clear();
        }
    }
}