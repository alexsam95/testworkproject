using System;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    interface ICommand
    {
        string PropertyBindName { set; }
        string BindMethodName { set; }
        PropertyResolveMode PropertyResolveMode { set; }
        ILifecycleStrategy LifecycleStrategy { set; }
        void AddObjectsForConstructor(params object[] parameters);
        void AddObjectsForMethod(string methodName, params object[] parameters);
        void AddObjectsForProperty(string propertyName, object parameter);

        void Invoke(bool withInstance);
    }
}