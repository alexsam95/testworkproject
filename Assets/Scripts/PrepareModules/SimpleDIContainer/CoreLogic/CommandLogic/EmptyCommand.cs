using System;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;

namespace SimpleDIContainer.CoreLogic.CommandLogic
{
    class EmptyCommand : ICommand
    {

        public string PropertyBindName { get; set; }
        public string BindMethodName { get; set; }
        public PropertyResolveMode PropertyResolveMode { get; set; }
        public ILifecycleStrategy LifecycleStrategy { get; set; }

        public void AddObjectsForConstructor(params object[] parameters)
        {

        }

        public void AddObjectsForMethod(string methodName, params object[] parameters)
        {
        }

        public void AddObjectsForProperty(string propertyName, object parameter)
        {
        }

        public void Invoke(bool withInstance)
        {
        }
    }
}