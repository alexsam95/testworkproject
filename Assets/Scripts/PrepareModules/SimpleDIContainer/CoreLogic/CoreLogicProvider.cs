using System.Collections.Generic;
using SimpleDIContainer.CoreLogic.CommandLogic;
using SimpleDIContainer.CoreLogic.Resolvers;
using SimpleDIContainer.CoreLogic.Strategy;
using SimpleDIContainer.DataLayer;
using SimpleDIContainer.SystemLayer;
using SimpleDIContainer.SystemLayer.Extensions;

namespace SimpleDIContainer.CoreLogic
{
    class CoreLogicProvider : ICoreLogicProvider
    {
        private IActivatorService _activatorService;
        private readonly IDataLayerProvider _dataLayerProvider;
        private ILifecycleStrategyFactory _lifecycleStrategyFactory;
        private ICommandController _commandController;
        private IEnumerable<IPreInstanceResolver> _preInstanceResolvers;

        public CoreLogicProvider(IDataLayerProvider dataLayerProvider)
        {
            VerificationExtensions.CheckParametersForNull(() =>
                    throw new ContainerException("CoreLogicProvider constructor argument is null", false)
                , dataLayerProvider);
            _dataLayerProvider = dataLayerProvider;
        }

        public ILifecycleStrategyFactory GetLifecycleController()
        {
            if (_lifecycleStrategyFactory != null) return _lifecycleStrategyFactory;
            _lifecycleStrategyFactory = new LifeCycleStrategyFactory(GetActivatorService());
            return _lifecycleStrategyFactory;
        }

        public ICommandController GetCommander()
        {
            if (_commandController != null) return _commandController;
            _commandController = new CommandController(_dataLayerProvider.GetStorage(), GetLifecycleController());
            return _commandController;
        }

        public IActivatorService GetActivatorService()
        {
            if (_activatorService != null) return _activatorService;
            _preInstanceResolvers = new[]
            {
                new ConstructorPreInstanceResolver(
                    _dataLayerProvider.GetStorage())
            };
            var postInstanceResolvers = new IPostInstanceResolver[]
            {
                new PropertyPreInstanceResolver(_dataLayerProvider.GetStorage()),
                new MethodPreInstanceResolver(_dataLayerProvider.GetStorage()),
                new FactoryResolver(_dataLayerProvider.GetStorage())
            };
            _activatorService = new ActivatorService(_preInstanceResolvers, postInstanceResolvers,
                new ActivatorErrorHandler());
            return _activatorService;
        }

        public void Dispose()
        {
            _commandController.Dispose();
            _preInstanceResolvers.ForEach(resolver => resolver.Dispose());
        }
    }
}