using UnityEngine;

namespace PrepareModules.ObjectPool
{
    public interface IObjectPool<T,TV> where T : MonoBehaviour
    {
        void LoadToPool(TV type, string path, int count);
        void ReturnToPool(T poolElement,TV type);
        T TakeInPool(TV type);
    }
}