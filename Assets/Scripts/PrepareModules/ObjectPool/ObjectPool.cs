using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace PrepareModules.ObjectPool
{
    public class ObjectPool<T, TV> : IObjectPool<T, TV> where T : MonoBehaviour
    {
        private readonly Dictionary<TV, Stack<T>> _poolRepository =
            new Dictionary<TV, Stack<T>>();

        public void LoadToPool(TV type, string path, int count)
        {
            var resource = Resources.Load<T>(path);
            if (resource == null)
            {
                Debug.LogError("Incorrect path:" + path);
                return;
            }

            var stack = new Stack<T>();
            for (var index = 0; index < count; index++)
            {
                CreateAndAddToStack(resource, stack);
            }

            AddToRepository(type, stack);
        }

        public void ReturnToPool(T poolElement, TV type)
        {
            poolElement.gameObject.SetActive(false);
            if (_poolRepository.ContainsKey(type))
            {
                _poolRepository[type].Push(poolElement);
            }
            else
            {
                var stack = new Stack<T>();
                stack.Push(poolElement);
                _poolRepository.Add(type, stack);
            }
        }

        public T TakeInPool(TV type)
        {
            if (_poolRepository.ContainsKey(type))
            {
                if (_poolRepository[type].Count != 0)
                {
                    var result = _poolRepository[type].Pop();
                    result.gameObject.SetActive(true);
                    return result;
                }

                Debug.LogError("need many elements in pool");
                return null;
            }

            Debug.LogError("type not registered");
            return null;
        }

        private void CreateAndAddToStack(T resource, Stack<T> stack)
        {
            var element = Object.Instantiate(resource);
            if (element == null)
            {
                Debug.LogError("error with create");
                return;
            }

            element.gameObject.SetActive(false);
            stack.Push(element);
        }

        private void AddToRepository(TV type, Stack<T> stack)
        {
            if (_poolRepository.ContainsKey(type))
            {
                var origStack = _poolRepository[type];
                foreach (var element in stack)
                {
                    origStack.Push(element);
                }
            }
            else
            {
                _poolRepository.Add(type, stack);
            }
        }
    }
}