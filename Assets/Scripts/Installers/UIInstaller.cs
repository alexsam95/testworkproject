﻿using PrepareModules.MVP;
using SimpleDIContainer.UserLayer;
using UI;
using UI.StartMenu;

namespace Installers
{
    public class UiInstaller : BaseInstaller
    {
        public UiInstaller(Container container) : base(container)
        {
        }

        public override void InstallDependencies()
        {
            Container.Bind<IPopupConstructor, PopupConstructor>().AsSingle();
            Container.Bind<IStartMenuModel, StartMenuModel>().AsSingle();
            Container.Bind<IStartMenuPresenter, StartMenuPresenter>().AsSingle();
            Container.BindInstance<UiPopupsContext>().AsSingle();
            Container.Bind<IUiLoader, UILoader>().AsSingle();
        }
    }
}
