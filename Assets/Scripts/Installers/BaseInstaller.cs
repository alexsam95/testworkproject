﻿using SimpleDIContainer.UserLayer;

namespace Installers
{
    public abstract class BaseInstaller
    {
        protected Container Container;

        public BaseInstaller(Container container)
        {
            Container = container;
        }

        public abstract void InstallDependencies();
    }
}
