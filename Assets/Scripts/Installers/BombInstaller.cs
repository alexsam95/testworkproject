﻿using Gameplay.BombLogic;
using Gameplay.Bombs;
using Gameplay.Bombs.BombLogic;
using PrepareModules.ObjectPool;
using SimpleDIContainer.UserLayer;

namespace Installers
{
    public class BombInstaller : BaseInstaller
    {
        private readonly ConfigurationProvider _configurationProvider;
        public BombInstaller(Container container, ConfigurationProvider configurationProvider) : base(container)
        {
            _configurationProvider = configurationProvider;
        }

        public override void InstallDependencies()
        {
            Container.BindInstance<DefaultBombExplodeComponent>().AsSingle()
                .SetToConstructor(BombType.UsualBomb, _configurationProvider.BombsConfig);
            Container.BindInstance<WithTimerExplodeComponent>().AsSingle()
                .SetToConstructor(BombType.TimerBomb, _configurationProvider.BombsConfig);
            Container.Bind<IBombRealizationsFactory, BombRealizationsFactory>().AsSingle().SetToConstructor(_configurationProvider.BombsConfig);
            Container
                .Bind<IObjectPool<BombVisualComponentsProvider, BombType>,
                    ObjectPool<BombVisualComponentsProvider, BombType>>()
                .AsSingle();
            Container.Bind<IBombExplodeInitializator, BombExplodeInitializator>().AsSingle();
            Container.BindInstance<BombPathFinder>().AsSingle();
            Container.Bind<IBombSpawner, BombSpawner>().AsSingle().SetToConstructor(_configurationProvider);
            Container.SetFactory<BombLogicBindFactory>().AsSingle().ActivateNonLazy();
        }
    }
}