﻿using PrepareModules.MVP;

namespace UI.StartMenu
{
    public class StartMenuPresenter : BasePresenter<StartMenuView>, IStartMenuPresenter
    {
        private readonly IStartMenuModel _model;
        public StartMenuPresenter(IStartMenuModel model) : base(PopupType.StartMenu)
        {
            _model = model;
        }
        public void StartGame()
        {
            _model.StartGame();
        }
    }
}