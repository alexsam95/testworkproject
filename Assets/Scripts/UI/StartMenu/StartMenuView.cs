﻿using PrepareModules.MVP;
using UnityEngine;
using UnityEngine.UI;

namespace UI.StartMenu
{
    public class StartMenuView : BaseView<IStartMenuPresenter>
    {
        [SerializeField] private Button _startButton;

        public override void SubscribeButtons()
        {
            _startButton.onClick.AddListener(StartGame);
        }

        private void StartGame()
        {
            Presenter.StartGame();
            Hide();
        }
    }
}