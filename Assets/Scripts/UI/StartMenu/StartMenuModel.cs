﻿using GameStateModule;

namespace UI.StartMenu
{
    public class StartMenuModel : IStartMenuModel
    {
        private readonly GameStateContainer _stateContainer;
        public StartMenuModel(GameStateContainer stateContainer)
        {
            _stateContainer = stateContainer;
        }

        public void StartGame()
        {
            _stateContainer.GameState.Value = GameState.InGame;
        }
    }
}
