﻿namespace UI.StartMenu
{
    public interface IStartMenuModel
    {
        void StartGame();
    }
}
