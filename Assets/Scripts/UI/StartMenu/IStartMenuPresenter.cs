﻿using PrepareModules.MVP;

namespace UI.StartMenu
{
    public interface IStartMenuPresenter : IPresenter<StartMenuView>
    {
        void StartGame();
    }
}