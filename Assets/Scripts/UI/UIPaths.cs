﻿namespace UI
{
    public static class UIPaths
    {
        public const string UiParentPath = "Prefabs/UI/UICanvas";
        public const string StartMenuPrefabPath = "Prefabs/UI/Popups/StartMenu";
    }
}
