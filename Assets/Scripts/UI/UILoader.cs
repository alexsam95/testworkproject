﻿using System;
using PrepareModules.MVP;
using UI.StartMenu;
using UnityEngine;
using Object = UnityEngine.Object;

namespace UI
{
    public class UILoader : IUiLoader
    {
        private readonly IPopupConstructor _popupConstructor;
        private readonly UiPopupsContext _popupsContext;
        private GameObject _uiParent;

        public UILoader(IPopupConstructor popupConstructor, UiPopupsContext popupsContext)
        {
            _popupConstructor = popupConstructor;
            _popupsContext = popupsContext;
        }

        public void LoadUi()
        {
            _uiParent = Object.Instantiate(Resources.Load(UIPaths.UiParentPath)) as GameObject;
            if (_uiParent == null)
            {
                throw new NullReferenceException("ui canvas is null");
            }

            _popupConstructor.ConstructPopup<StartMenuView, IStartMenuPresenter>(UIPaths.StartMenuPrefabPath, _uiParent,
                _popupsContext.StartMenuPresenter);
        }

        public void Dispose()
        {
            if (_uiParent != null)
            {
                Object.Destroy(_uiParent);
            }
        }
    }
}
