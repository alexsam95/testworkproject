﻿using UI.StartMenu;

namespace UI
{
    public class UiPopupsContext
    {
        public IStartMenuModel StartMenuModel { get; }
        public IStartMenuPresenter StartMenuPresenter { get; }

        public UiPopupsContext(IStartMenuModel startMenuModel, IStartMenuPresenter startMenuPresenter)
        {
            StartMenuPresenter = startMenuPresenter;
            StartMenuModel = startMenuModel;
        }
    }
}