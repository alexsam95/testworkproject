﻿using System;

namespace UI
{
    public interface IUiLoader : IDisposable
    {
        void LoadUi();
    }
}