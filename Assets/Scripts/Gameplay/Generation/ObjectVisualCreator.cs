﻿using UnityEngine;
using Object = UnityEngine.Object;

namespace Gameplay.Generation
{
    public class ObjectVisualCreator : IObjectVisualCreator
    {
        private readonly ObjectVisualPathFinder _objectVisualPathFinder;
        private GameObject _parent;

        public ObjectVisualCreator(ObjectVisualPathFinder objectVisualPathFinder)
        {
            _objectVisualPathFinder = objectVisualPathFinder;
        }

        public void SubscribeToGenerationProcess(IObjectsMapGenerator objectsMapGenerator)
        {
            objectsMapGenerator.OnElementGenerate += CreateVisual;
        }
        public void UnSubscribeToGenerationProcess(IObjectsMapGenerator objectsMapGenerator)
        {
            objectsMapGenerator.OnElementGenerate += CreateVisual;
        }

        public void Dispose()
        {
            Object.Destroy(_parent);
        }

        public void CreateVisual(GameObjectType type, Vector3Int position)
        {
            if (_parent == null || !_parent)
            {
                _parent = new GameObject();
            }

            if (type == GameObjectType.None) return;
            var gameObj = Object.Instantiate(Resources.Load<GameObject>(_objectVisualPathFinder.FindVisualPath(type)),
                _parent.transform);
            gameObj.transform.position = position;
        }
    }
}