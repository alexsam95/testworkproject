﻿using System;
using UnityEngine;

namespace Gameplay.Generation
{
    public interface IObjectVisualCreator : IDisposable
    {
        void SubscribeToGenerationProcess(IObjectsMapGenerator objectsMapGenerator);
        void UnSubscribeToGenerationProcess(IObjectsMapGenerator objectsMapGenerator);
        void CreateVisual(GameObjectType type, Vector3Int position);
    }
}