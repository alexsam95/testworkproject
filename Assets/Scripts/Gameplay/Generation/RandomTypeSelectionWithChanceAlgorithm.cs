﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gameplay.Generation;
using Random = UnityEngine.Random;

namespace Gameplay
{
    public class RandomTypeSelectionWithChanceAlgorithm : IRandomTypeSelectionAlgorithm
    {
        private List<GameObjectType> _elements = new List<GameObjectType>();
        private readonly ChanceConfig _chanceConfig;

        public RandomTypeSelectionWithChanceAlgorithm(ChanceConfig chanceConfig)
        {
            _chanceConfig = chanceConfig;
        }

        public void GenerateData()
        {
            Dispose();
            var total = 100;
            foreach (var element in _chanceConfig.GetElementsChance())
            {
                total -= element.Value;
                for (var i = 0; i < element.Value; i++)
                {
                    _elements.Add(element.Key);
                }
            }

            if (total < 0)
            {
                throw new Exception("chance sum > 100");
            }

            for (var i = 0; i < total; i++)
            {
                _elements.Add(GameObjectType.None);
            }

            var shuffledResult = _elements.OrderBy(x => Guid.NewGuid()).ToList();
            _elements = shuffledResult;
        }

        public GameObjectType GetRandomType()
        {
            return _elements[Random.Range(0, _elements.Count)];
        }

        public void Dispose()
        {
            _elements.Clear();
        }
    }
}