﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Generation
{
    [CreateAssetMenu(fileName = "ChanceData", menuName = "CreateChanceData", order = 0)]
    public class ChanceConfig : ScriptableObject
    {
        [SerializeField] private int _wallChance;
        [SerializeField] private int _heroChance;
        private Dictionary<GameObjectType, int> _elementsChanceDictionary;

        public Dictionary<GameObjectType, int> GetElementsChance()
        {
            if (_elementsChanceDictionary != null) return _elementsChanceDictionary;
            ValidateChanceTotal();
            _elementsChanceDictionary = new Dictionary<GameObjectType, int>
            {
                {GameObjectType.Wall, _wallChance}, {GameObjectType.Hero, _heroChance}
            };
            return _elementsChanceDictionary;
        }

        private void ValidateChanceTotal()
        {
            if (_wallChance + _heroChance > 100)
            {
                throw new Exception("Error:total chance>100");
            }
        }
    }
}