﻿using System;

namespace Gameplay.Generation
{
    public class ObjectVisualPathFinder
    {
        public string FindVisualPath(GameObjectType type)
        {
            switch (type)
            {
                case GameObjectType.None:
                    return "";
                case GameObjectType.Wall:
                    return GameObjectsPaths.WallObjectPath;
                case GameObjectType.Hero:
                    return GameObjectsPaths.HeroObjectPath;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}