﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Generation
{
    public interface IObjectsMapGenerator
    {
        event Action<GameObjectType, Vector3Int> OnElementGenerate;
        Dictionary<Vector3, GameObjectType> GenerateMap(Vector3Int startPosition, Vector3Int endPosition);
    }
}