﻿using System;

namespace Gameplay.Generation
{
    public interface IRandomTypeSelectionAlgorithm : IDisposable
    {
        void GenerateData();
        GameObjectType GetRandomType();
    }
}