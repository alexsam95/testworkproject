﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Gameplay.Generation
{
    public class ObjectsMapGenerator : IObjectsMapGenerator
    {
        public event Action<GameObjectType, Vector3Int> OnElementGenerate;
        private readonly IRandomTypeSelectionAlgorithm _randomWithChanceAlgorithm;

        public ObjectsMapGenerator(IRandomTypeSelectionAlgorithm randomWithChanceAlgorithm)
        {
            _randomWithChanceAlgorithm = randomWithChanceAlgorithm;
        }

        public Dictionary<Vector3, GameObjectType> GenerateMap(Vector3Int startPosition, Vector3Int endPosition)
        {
            _randomWithChanceAlgorithm.GenerateData();
            var map = new Dictionary<Vector3, GameObjectType>();
            for (var i = startPosition.x; i <= endPosition.x; i++)
            {
                for (var j = startPosition.z; j >= endPosition.z; j--)
                {
                    var type = _randomWithChanceAlgorithm.GetRandomType();
                    var position = new Vector3Int(i, endPosition.y, j);
                    if (map.ContainsKey(position))
                    {
                        map[position] = type;
                    }
                    else
                    {
                        map.Add(position, type);
                    }

                    OnElementGenerate?.Invoke(type, position);
                }
            }

            return map;
        }
    }
}