﻿namespace Gameplay
{
    public class GameObjectsPaths
    {
        public const string HeroObjectPath = "Prefabs/Gameplay/Hero";
        public const string WallObjectPath = "Prefabs/Gameplay/Wall";
        public const string BombObjectPath = "Prefabs/Gameplay/Bomb";
        public const string BombTimerPath = "Prefabs/Gameplay/BombWithTimer";
    }
}