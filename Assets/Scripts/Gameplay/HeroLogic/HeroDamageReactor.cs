﻿using Gameplay.HeroLogic;
using UnityEngine;

namespace Gameplay
{
    public class HeroDamageReactor : MonoBehaviour, IHeroEntity
    {
        private int _hp;
        public void React(int damageValue)
        {
            _hp -= damageValue;
            if (_hp <= 0)
            {
                gameObject.SetActive(false);
            }
        }

        public void SetStartHp(uint value)
        {
            _hp = (int)value;
        }
        private void Start()
        {
            HeroInitializator.RegistrateHero(this);
        }
    }
}