﻿using UnityEngine;

namespace Gameplay.HeroLogic
{
    [CreateAssetMenu(fileName = "HeroData", menuName = "CreateHeroData", order = 0)]
    public class HeroConfig : ScriptableObject
    {
        [SerializeField] private uint _usualHeroHp;
        public uint UsualHeroHp => _usualHeroHp;
    }
}