﻿namespace Gameplay.HeroLogic
{
    public class HeroInitializator
    {
        private static HeroConfig _heroConfig;
        public HeroInitializator(HeroConfig heroConfig)
        {
            _heroConfig = heroConfig;
        }
        public static void RegistrateHero(IHeroEntity heroEntity)
        {
            heroEntity.SetStartHp(_heroConfig.UsualHeroHp);
        }
    }
}