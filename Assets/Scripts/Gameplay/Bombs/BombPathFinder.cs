﻿using System;
using Gameplay.BombLogic;

namespace Gameplay.Bombs
{
    public class BombPathFinder
    {
        public string FindObjectPath(BombType bombType)
        {
            switch (bombType)
            {
                case BombType.UsualBomb:
                    return GameObjectsPaths.BombObjectPath;
                case BombType.TimerBomb:
                    return GameObjectsPaths.BombTimerPath;
                default:
                    throw new ArgumentOutOfRangeException(nameof(bombType), bombType, null);
            }
        }
    }
}