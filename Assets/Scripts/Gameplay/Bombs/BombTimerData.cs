﻿using Gameplay.BombLogic;

namespace Gameplay.Bombs
{
    public class BombTimerData : BombData
    {
        public float TimerValue { get;}
        public BombTimerData(uint bombDamage, uint bombSpawnInterval, uint bombExplodeRadius, float timerValue) : base(bombDamage, bombSpawnInterval, bombExplodeRadius)
        {
            TimerValue = timerValue;
        }
    }
}