﻿
namespace Gameplay.Bombs.BombLogic
{
    public class BombModulesMediator
    {
        /// <summary>
        /// Container for bomb logic modules, for example: ExplodeLogic, DamageLogic etc
        /// </summary>
        protected readonly IExplodeComponent ExplodeLogic;

        public BombModulesMediator(IExplodeComponent explodeComponent)
        {
            ExplodeLogic = explodeComponent;
        }
        public virtual void Explode(BombVisualComponentsProvider bombVisualComponentsProvider)
        {
            ExplodeLogic?.Explode(bombVisualComponentsProvider);;
        }
    }
}