﻿using System.Collections.Generic;
using Gameplay.BombLogic;
using UniRx;
using UniRx.Triggers;

namespace Gameplay.Bombs.BombLogic
{
    public class BombExplodeInitializator : IBombExplodeInitializator
    {
        private readonly Dictionary<BombType, BombModulesMediator> _realizations;

        public BombExplodeInitializator(Dictionary<BombType, BombModulesMediator> realizations)
        {
            _realizations = realizations;
        }

        public void SubscribeToSpawn(IBombSpawner bombSpawner)
        {
            bombSpawner.OnBombSpawned += InitializeBombExplode;
        }

        public void UnsubscribeToSpawn(IBombSpawner bombSpawner)
        {
            bombSpawner.OnBombSpawned -= InitializeBombExplode;
        }

        public void InitializeBombExplode(BombType bombType, BombVisualComponentsProvider bombVisualComponentsProvider)
        {
            var concreteRealizations = _realizations[bombType];
            var inCollisionProcess = false;
            bombVisualComponentsProvider.OnCollisionStayAsObservable().TakeUntilDisable(bombVisualComponentsProvider)
                .Subscribe(_ =>
                {
                    if (inCollisionProcess) return;
                    concreteRealizations.Explode(bombVisualComponentsProvider);
                    inCollisionProcess = true;
                });
        }
    }
}