﻿using System;
using System.Collections.Generic;
using System.Linq;
using Gameplay.BombLogic;

namespace Gameplay.Bombs.BombLogic
{
    public class BombRealizationsFactory : IBombRealizationsFactory
    {
        private readonly BombsConfig _bombsConfig;
        private readonly DefaultBombExplodeComponent _defaultBombExplodeComponent;
        private readonly WithTimerExplodeComponent _withTimerExplodeComponent;

        public BombRealizationsFactory(BombsConfig bombsConfig, DefaultBombExplodeComponent defaultBombExplodeComponent, WithTimerExplodeComponent withTimerExplodeComponent)
        {
            _bombsConfig = bombsConfig;
            _withTimerExplodeComponent = withTimerExplodeComponent;
            _defaultBombExplodeComponent = defaultBombExplodeComponent;
        }

        public Dictionary<BombType, BombModulesMediator> GenerateBombRealizations()
        {
            return _bombsConfig.BombsDataRepository.ToDictionary(dataSet => dataSet.Key,
                dataSet => new BombModulesMediator(FindExplodeComponent(dataSet.Key)));
        }

        private IExplodeComponent FindExplodeComponent(BombType type)
        {
            switch (type)
            {
                case BombType.UsualBomb:
                    return _defaultBombExplodeComponent;
                case BombType.TimerBomb:
                    return _withTimerExplodeComponent;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
    }
}