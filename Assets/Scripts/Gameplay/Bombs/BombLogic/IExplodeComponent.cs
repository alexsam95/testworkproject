﻿using Gameplay.BombLogic;

namespace Gameplay.Bombs.BombLogic
{
    public interface IExplodeComponent
    {
        void Explode(BombVisualComponentsProvider bombVisualComponentsProvider);
    }
}