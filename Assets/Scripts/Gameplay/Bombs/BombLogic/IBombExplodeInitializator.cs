﻿using Gameplay.BombLogic;

namespace Gameplay.Bombs.BombLogic
{
    public interface IBombExplodeInitializator 
    {
        void SubscribeToSpawn(IBombSpawner bombSpawner);
        void UnsubscribeToSpawn(IBombSpawner bombSpawner);
        void InitializeBombExplode(BombType bombType, BombVisualComponentsProvider bombVisualComponentsProvider);
    }
}