﻿using System;
using Gameplay.BombLogic;
using PrepareModules.ObjectPool;
using UniRx;

namespace Gameplay.Bombs.BombLogic
{
    public class WithTimerExplodeComponent : DefaultBombExplodeComponent
    {
        public WithTimerExplodeComponent(BombType bombType, BombsConfig bombsConfig,
            IObjectPool<BombVisualComponentsProvider, BombType> pool)
            : base(bombType, bombsConfig, pool)
        {
        }

        public override void Explode(BombVisualComponentsProvider bombVisualComponentsProvider)
        {
            Observable.Timer(TimeSpan.FromSeconds(BombsConfig.BombTimerData.TimerValue))
                .TakeUntilDisable(bombVisualComponentsProvider).Subscribe(_ =>
                {
                    base.Explode(bombVisualComponentsProvider);
                });
        }
    }
}