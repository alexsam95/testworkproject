﻿using System.Collections.Generic;
using Gameplay.BombLogic;
using SimpleDIContainer.UserLayer;

namespace Gameplay.Bombs.BombLogic
{
    public class BombLogicBindFactory : BindFactory<Dictionary<BombType, BombModulesMediator>>
    {
        private readonly IBombRealizationsFactory _bombRealizationsFactory;
        public BombLogicBindFactory(IBombRealizationsFactory bombRealizationsFactory)
        {
            _bombRealizationsFactory = bombRealizationsFactory;
        }
        protected override Dictionary<BombType, BombModulesMediator> ConstructObject()
        {
            return _bombRealizationsFactory.GenerateBombRealizations();
        }
    }
}