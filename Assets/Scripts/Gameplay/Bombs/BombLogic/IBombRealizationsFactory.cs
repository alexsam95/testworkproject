﻿using System.Collections.Generic;
using Gameplay.BombLogic;

namespace Gameplay.Bombs.BombLogic
{
    public interface IBombRealizationsFactory
    {
        Dictionary<BombType, BombModulesMediator> GenerateBombRealizations();
    }
}