﻿using Gameplay.BombLogic;
using PrepareModules.ObjectPool;
using UnityEngine;

namespace Gameplay.Bombs.BombLogic
{
    public class DefaultBombExplodeComponent : IExplodeComponent
    {
        private readonly IObjectPool<BombVisualComponentsProvider, BombType> _pool;
        protected readonly BombsConfig BombsConfig;
        private readonly BombType _bombType;

        public DefaultBombExplodeComponent(BombType bombType, BombsConfig bombsConfig,
            IObjectPool<BombVisualComponentsProvider, BombType> pool)
        {
            _bombType = bombType;
            _pool = pool;
            BombsConfig = bombsConfig;
        }

        public virtual void Explode(BombVisualComponentsProvider bombVisualComponentsProvider)
        {
            var currentData = BombsConfig.BombsDataRepository[_bombType];
            bombVisualComponentsProvider.Rigidbody.velocity = Vector3.zero;
            var hits = Physics.OverlapSphere(bombVisualComponentsProvider.transform.position,
                currentData.BombExplodeRadius, 1 << 8);
            if (hits.Length == 0)
            {
                _pool.ReturnToPool(bombVisualComponentsProvider, _bombType);
                return;
            }
            foreach (var collider in hits)
            {
                if (collider == null) continue;
                var reactor = collider.gameObject.GetComponent<HeroDamageReactor>();
                if (reactor == null) continue;
                var origin = collider.gameObject.transform.position;
                if (!Physics.Raycast(origin, (bombVisualComponentsProvider.transform.position - origin).normalized,
                    out var hit,
                    Vector3.Distance(origin, bombVisualComponentsProvider.transform.position))) continue;
                if (hit.transform.gameObject == bombVisualComponentsProvider.gameObject)
                {
                    reactor.React((int) currentData.BombDamage);
                }
            }
            _pool.ReturnToPool(bombVisualComponentsProvider, _bombType);
        }
    }
}