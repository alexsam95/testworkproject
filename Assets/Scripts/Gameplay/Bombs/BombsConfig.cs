﻿using System.Collections.Generic;
using Gameplay.BombLogic;
using UnityEngine;

namespace Gameplay.Bombs
{
    [CreateAssetMenu(fileName = "BombsConfig", menuName = "CreateBombsConfig", order = 0)]
    public class BombsConfig : ScriptableObject
    {
        [SerializeField] private uint _usualBombDamage;
        [SerializeField] private uint _usualBombExplodeRadius;
        [SerializeField] private uint _usualBombSpawnInterval;
        [SerializeField] private uint _timerBombDamage;
        [SerializeField] private uint _timerBombExplodeRadius;
        [SerializeField] private uint _timerBombSpawnInterval;
        [SerializeField] private float _timerBombTimerValue;
        [SerializeField] private float _spawnHeight;
        public float SpawnHeight => _spawnHeight;
        public IReadOnlyDictionary<BombType, BombData> BombsDataRepository { get; private set; }
        public BombTimerData BombTimerData { get; private set; }

        public void Initialize()
        {
            BombTimerData = new BombTimerData(_timerBombDamage, _timerBombSpawnInterval, _timerBombExplodeRadius,
                _timerBombTimerValue);
            BombsDataRepository = new Dictionary<BombType, BombData>
            {
                {BombType.UsualBomb, new BombData(_usualBombDamage, _usualBombSpawnInterval, _usualBombExplodeRadius)},
                {BombType.TimerBomb, BombTimerData}
            };
        }
    }
}