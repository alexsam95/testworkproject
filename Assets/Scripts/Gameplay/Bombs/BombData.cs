﻿namespace Gameplay.BombLogic
{
    public class BombData
    {
        public uint BombDamage { get; }
        public uint BombSpawnInterval { get; }
        public uint BombExplodeRadius { get; }

        public BombData(uint bombDamage, uint bombSpawnInterval, uint bombExplodeRadius)
        {
            BombDamage = bombDamage;
            BombSpawnInterval = bombSpawnInterval;
            BombExplodeRadius = bombExplodeRadius;
        }
    }
}