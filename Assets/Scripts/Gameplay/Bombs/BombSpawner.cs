﻿using System;
using Gameplay.BombLogic;
using PrepareModules.ObjectPool;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Gameplay.Bombs
{
    public class BombSpawner : IBombSpawner
    {
        private const int DefaultCountElementsInPool = 5;
        private readonly IObjectPool<BombVisualComponentsProvider, BombType> _objectPool;
        private readonly ConfigurationProvider _configurationProvider;
        private readonly CompositeDisposable _compositeDisposable = new CompositeDisposable();
        private readonly BombsConfig _bombsConfig;
        private readonly BombPathFinder _bombPathFinder;
        public event Action<BombType, BombVisualComponentsProvider> OnBombSpawned;

        public BombSpawner(ConfigurationProvider provider, IObjectPool<BombVisualComponentsProvider, BombType> pool,
            BombPathFinder bombPathFinder)
        {
            _bombsConfig = provider.BombsConfig;
            _configurationProvider = provider;
            _objectPool = pool;
            _bombPathFinder = bombPathFinder;
        }

        public void StartSpawn()
        {
            foreach (var bombDataSet in _bombsConfig.BombsDataRepository)
            {
                _objectPool.LoadToPool(bombDataSet.Key, _bombPathFinder.FindObjectPath(bombDataSet.Key),
                    DefaultCountElementsInPool);
                Observable.Interval(TimeSpan.FromSeconds(bombDataSet.Value.BombSpawnInterval))
                    .Subscribe(_ => { Spawn(bombDataSet.Key); })
                    .AddTo(_compositeDisposable);
            }
        }

        public void StopSpawn()
        {
            _compositeDisposable.Clear();
        }

        private void Spawn(BombType bombType)
        {
            var startPosition = _configurationProvider.StartGenerationPosition;
            var endPosition = _configurationProvider.EndGenerationPosition;
            var randX = Random.Range(startPosition.x, endPosition.x);
            var randZ = Random.Range(startPosition.z, endPosition.z);
            var bomb = _objectPool.TakeInPool(bombType);
            bomb.transform.position = new Vector3(randX, _bombsConfig.SpawnHeight, randZ);
            OnBombSpawned?.Invoke(bombType, bomb);
        }
    }
}