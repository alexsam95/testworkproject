﻿using System;
using Gameplay.BombLogic;

namespace Gameplay.Bombs
{
    public interface IBombSpawner
    {
        event Action<BombType, BombVisualComponentsProvider> OnBombSpawned;
        void StartSpawn();
        void StopSpawn();
    }
}