﻿using UnityEngine;

namespace Gameplay.Bombs
{
    [RequireComponent(typeof(Rigidbody))]
    public class BombVisualComponentsProvider : MonoBehaviour
    {
        [SerializeField] private Rigidbody _rigidbody;
        public Rigidbody Rigidbody => _rigidbody;
    }
}
