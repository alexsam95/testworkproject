﻿using System;
using Gameplay.BombLogic;
using Gameplay.Bombs;
using Gameplay.Bombs.BombLogic;
using Gameplay.Generation;
using GameStateModule;
using UI;
using UniRx;

namespace Gameplay
{
    public class GameLoader : IDisposable
    {
        private readonly IObjectsMapGenerator _objectsMapGenerator;
        private readonly ConfigurationProvider _configurationProvider;
        private readonly IBombSpawner _bombSpawner;
        private readonly IObjectVisualCreator _objectVisualCreator;
        private readonly IBombExplodeInitializator _bombExplodeInitializator;

        public GameLoader(ConfigurationProvider configurationProvider, GameStateContainer gameStateContainer,
            IObjectsMapGenerator objectsMapGenerator, IBombSpawner bombSpawner, IUiLoader uiLoader,
            IObjectVisualCreator visualCreator, IBombExplodeInitializator bombExplodeInitializator)
        {
            _bombExplodeInitializator = bombExplodeInitializator;
            _objectVisualCreator = visualCreator;
            _configurationProvider = configurationProvider;
            _objectsMapGenerator = objectsMapGenerator;
            _bombSpawner = bombSpawner;
            uiLoader.LoadUi();
            gameStateContainer.GameState.Subscribe(LoadGame);
        }

        private void LoadGame(GameState state)
        {
            if (state != GameState.InGame) return;
            _objectVisualCreator.SubscribeToGenerationProcess(_objectsMapGenerator);
            _objectsMapGenerator.GenerateMap(_configurationProvider.StartGenerationPosition,
                _configurationProvider.EndGenerationPosition);
            _bombExplodeInitializator.SubscribeToSpawn(_bombSpawner);
            _bombSpawner.StartSpawn();
        }

        public void Dispose()
        {
            _objectVisualCreator.UnSubscribeToGenerationProcess(_objectsMapGenerator);
            _objectVisualCreator.Dispose();
            _bombExplodeInitializator.UnsubscribeToSpawn(_bombSpawner);
            _bombSpawner.StopSpawn();
        }
    }
}