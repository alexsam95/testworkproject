﻿using Gameplay.BombLogic;
using Gameplay.Bombs;
using Gameplay.Generation;
using Gameplay.HeroLogic;
using UnityEngine;

public class ConfigurationProvider : MonoBehaviour
{
    [SerializeField] private HeroConfig _heroConfig;
    [SerializeField] private BombsConfig _bombsConfig;
    [SerializeField] private ChanceConfig _chanceConfig;
    [SerializeField] private GameObject _startGenerationPositionPoint;
    [SerializeField] private GameObject _endGenerationPositionPoint;
    public HeroConfig HeroConfig => _heroConfig;
    public BombsConfig BombsConfig => _bombsConfig;
    public ChanceConfig ChanceConfig => _chanceConfig;

    public Vector3Int StartGenerationPosition
    {
        get
        {
            var position = _startGenerationPositionPoint.transform.position;
            return new Vector3Int((int) position.x,
                (int) position.y, (int) position.z);
        }
    }

    public Vector3Int EndGenerationPosition
    {
        get
        {
            var position = _endGenerationPositionPoint.transform.position;
            return new Vector3Int((int) position.x,
                (int) position.y, (int) position.z);
        }
    }
}
